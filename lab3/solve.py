import random

f = open("input.txt", "r")

data1 = f.read()

data = data1.split(", ")

def checkValid(a, b):
    if (len(a) >= 3 and len(b) >= 3 and 8 <= len(a) + len(b) <= 10):
        return True
    else:
        return False

def generateWord(data):
    a = data[random.randint(0, len(data) - 1)]
    size = random.randint(3, 7)
    return a[:size]

def generatePass():

    while (True):
        a = generateWord(data)
        b = generateWord(data)

        if (checkValid(a, b)):
            return str(a) + str(b)

