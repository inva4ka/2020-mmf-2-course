# Сергей Инвалев, 2 курс, 7 группа.
--------------------
* ### Инд. зад. №1 [вариант 6](https://bitbucket.org/inva4ka/2020-mmf-2-course/src/master/lab1/)
* ### Инд. зад. №2 [вариант 6](https://bitbucket.org/inva4ka/2020-mmf-2-course/src/master/lab2/)
* ### Инд. зад. №3 [вариант 6](https://bitbucket.org/inva4ka/2020-mmf-2-course/src/master/lab3/)
* ### [optimized](https://bitbucket.org/inva4ka/2020-mmf-2-course/src/master/optimized/)
* ### [unoptimized](https://bitbucket.org/inva4ka/2020-mmf-2-course/src/master/unoptimized/)
* ### [Сайт с визуализацией](https://bitbucket.org/inva4ka/2020-mmf-2-course/src/master/site%20with%20vizualization/)
* ### [Сайт с визуализацией на heroku](https://hotelinvaleva.herokuapp.com/)




