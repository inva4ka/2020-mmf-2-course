from datetime import datetime, date, timedelta
import timeit
import time 
from flask import Flask, render_template, redirect, url_for,request



app = Flask(__name__)

file = "out100.csv"
dataFormat = '%Y-%m-%d %H:%M:%S'

def parseUserInput(StartDateTime, EndDateTime, ResourseID = -1):

    StartDateTime = date(int(StartDateTime[0:4]), int(StartDateTime[5:7]), int(StartDateTime[8:10]))
    StartDateTime = int(time.mktime(StartDateTime.timetuple())) 
    

    EndDateTime = date(int(EndDateTime[0:4]), int(EndDateTime[5:7]), int(EndDateTime[8:10]))
    EndDateTime = int(time.mktime(EndDateTime.timetuple())) 
    
    if (ResourseID != -1):
        return [ResourseID, StartDateTime, EndDateTime]
    else:
        return [StartDateTime, EndDateTime]



def read(file):
    f = open(file)
    
    data = f.read()

    f.close()

    return data


def parseString(str):
    return str.split(",")


def printArray(arr):
    for i in arr:
        print(i)


def parseInput(data, dataFormat):

    dict = {}

    data = data.split('\n')
        
    for i in range(1, len(data) - 1):

        data[i] = data[i].split(',')
        data[i][1] = data[i][1].replace('T', ' ')[:data[i][1].rfind('.')]
        data[i][1] = datetime.strptime(data[i][1], dataFormat)

        data[i][2] = data[i][2].replace('T', ' ')[:data[i][2].rfind('.')]
        data[i][2] = datetime.strptime(data[i][2], dataFormat)

        unix1 = int(time.mktime(data[i][1].timetuple()))
        unix2 = int(time.mktime(data[i][2].timetuple())) 

        st = str(unix1) + "," + str(unix2)

        if data[i][0] in dict:
            dict.update({data[i][0]:dict[data[i][0]] + "," + st})
        else:
            dict.update({data[i][0]:st})
       
    return dict


dict = parseInput(read(file), dataFormat)


@app.route("/", methods=["POST", "GET"])
def login():
    if request.method == "POST":
        id = request.form.get("ID")
        start = request.form.get("checkInStart")
        end = request.form.get("checkInEnd")
        
        if request.form.get('sybmit') == 'sybmit':
            if (id): 
                return redirect(url_for("findById", ResourseID=id, StartDateTime=start, EndDateTime = end))
            else:
                return redirect(url_for("searchId",StartDateTime=start, EndDateTime = end))
        else:
            tmp = parseUserInput(start, end)
            START = datetime.fromtimestamp(int(tmp[0])).strftime('%Y-%m-%d')
            END = datetime.fromtimestamp(int(tmp[1])).strftime('%Y-%m-%d')
            return redirect(url_for("table", StartDateTime = START, EndDateTime = END))
            
    else:
        return render_template("hotel.html", ans = "")

@app.route("/table/<StartDateTime>&<EndDateTime>", methods=["POST", "GET"])
def table(StartDateTime, EndDateTime):
    arrayDate = giveArrayDate(StartDateTime, EndDateTime)
    ans = isFreeOrBusy(StartDateTime, EndDateTime)
    return render_template("table_hotel.html", arrayDate = arrayDate, ans = ans, lenAns = len(ans), lenAnsI = len(ans[1]))
        

@app.route('/searchId/<StartDateTime>&<EndDateTime>', methods=["POST", "GET"])
def searchId(StartDateTime, EndDateTime):
    
    checkInStart = request.form.get("checkInStart")
    if (checkInStart):
        id = request.form.get("ID")
        start = request.form.get("checkInStart")
        end = request.form.get("checkInEnd")

        if request.form.get('sybmit') == 'sybmit':           
            if (id): 
                return redirect(url_for("findById", ResourseID=id, StartDateTime=start, EndDateTime = end))
            else:
                return redirect(url_for("searchId", StartDateTime=start, EndDateTime = end))
        else:
            tmp = parseUserInput(start, end)
            START = datetime.fromtimestamp(int(tmp[0])).strftime('%Y-%m-%d')
            END = datetime.fromtimestamp(int(tmp[1])).strftime('%Y-%m-%d')
            return redirect(url_for("table", StartDateTime = START, EndDateTime = END))


    ids = set()
    closed = set()

    tmp = parseUserInput(StartDateTime, EndDateTime)
   
    for i in dict:
        arr = dict[str(i)].split(",")
        for val in range(0, len(arr), 2):
            if (i in closed) or check([int(arr[val]), int(arr[val + 1])], tmp[0], tmp[1]):
                closed.add(i)
                if val in ids:
                    ids.remove(i)
            else:
                ids.add(i)
    str1 = " "
    for i in ids:
        str1 += i + ","
    str1= str1[:-1]

    return render_template("hotel.html", ans = str(str1)) 


@app.route('/findById/<ResourseID>&<StartDateTime>&<EndDateTime>', methods=["POST", "GET"])
def findById(ResourseID, StartDateTime, EndDateTime):
    
    checkInStart = request.form.get("checkInStart")
    if (checkInStart):
        id = request.form.get("ID")
        start = request.form.get("checkInStart")
        end = request.form.get("checkInEnd")

        if request.form.get('sybmit') == 'sybmit':
            if (id): 
                return redirect(url_for("findById", ResourseID=id, StartDateTime=start, EndDateTime = end))
            else:
                return redirect(url_for("searchId",StartDateTime=start, EndDateTime = end))
        else:
            tmp = parseUserInput(start, end)
            START = datetime.fromtimestamp(int(tmp[0])).strftime('%Y-%m-%d')
            END = datetime.fromtimestamp(int(tmp[1])).strftime('%Y-%m-%d')
            return redirect(url_for("table", StartDateTime = START, EndDateTime = END))
    
    tmp = parseUserInput(StartDateTime, EndDateTime, ResourseID)
    if tmp[0] in dict: 
        arr = dict[tmp[0]].split(",")
    else: 
        render_template("hotel.html", ans = "Занято")

    ans = "Свободно"
    
    for i in range(0, len(arr), 2):
        if (check([int(arr[i]), int(arr[i + 1])], tmp[1], tmp[2])):
            ans = "Занято"
            break
    return render_template("hotel.html", ans = ans) 


def check(hotel, dateStart, dateEnd):
  return (
    (hotel[0] <= dateStart < hotel[1]) 
    or  
    (hotel[0] < dateEnd <= hotel[1]) 
    or
    (dateStart <= hotel[0] < dateEnd) 
    or
    (dateStart < hotel[1] <= dateEnd)
  )

def isFreeOrBusy(StartDateTime, EndDateTime):
    StartDateTime = date(int(StartDateTime[0:4]), int(StartDateTime[5:7]), int(StartDateTime[8:10]))
    EndDateTime = date(int(EndDateTime[0:4]), int(EndDateTime[5:7]), int(EndDateTime[8:10])) 

    ans = []

    for id in dict:
        arr = dict[id].split(",")
        tmp = []
        tmp.append(str(id))
        start = StartDateTime
        
        while (start != EndDateTime):
            oneDay = timedelta(1)
            start += oneDay

            day = start
            day = int(time.mktime(start.timetuple())) 

            flag = True

            for i in range(0, len(arr), 2):
                tempStart = arr[i]
                tempEnd = arr[i + 1]

                if (not checkPoint(int(tempStart), int(tempEnd), day)):
                    flag = False
                    break

            tmp.append(flag)
        
        ans.append(tmp)
   
    return ans

def checkPoint(st, ed, point):
    if (st <= point and point <= ed):
        return False
    else:
        return True

def giveArrayDate(StartDateTime, EndDateTime):
    StartDateTime = date(int(StartDateTime[0:4]), int(StartDateTime[5:7]), int(StartDateTime[8:10]))
    EndDateTime = date(int(EndDateTime[0:4]), int(EndDateTime[5:7]), int(EndDateTime[8:10]))
    
    arrayDate = []
    if (StartDateTime > EndDateTime):
        return []

    while (True) :
        oneDay = timedelta(1)
        StartDateTime += oneDay
        arrayDate.append(str(StartDateTime))
        if (StartDateTime == EndDateTime):
            break

    return arrayDate


if __name__ == "__main__":
    app.run(debug = True)
