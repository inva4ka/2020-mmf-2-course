import Test
from solve import findValues
Test.assert_equals(findValues([1, 2, 3, 4, 5, 6], 5), 1, 4)
Test.assert_equals(findValues([1, 1, 1, 1, 1, 1], 2), 1, 2)
Test.assert_equals(findValues([1, 4, 6, 7, 2], 3),    1, 5)
Test.assert_equals(findValues([0, 1, 5, 4], 5),       1, 3)
Test.assert_equals(findValues([2, 3], 5),             1, 2)
Test.assert_equals(findValues([6, 1, 5, 6], 8),       1, 5)
Test.assert_equals(findValues([0, 1, 9, 1, 2], 2),    2, 4)
Test.assert_equals(findValues([1, 4, 6, 7, 2], 3),    1, 2)
Test.assert_equals(findValues([5, 3, 2, 4, 5, 6], 3), 1, 5)
Test.assert_equals(findValues([1, 1, 1, 1, 1, 1], 2), 1, 5)
