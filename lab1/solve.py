def oddish_or_evenish(val):
    sum = 0

    while (val > 0):
        sum += val % 10
        val //= 10

    if (sum % 2 == 1):
        return "Oddish"
    else:
        return "Evenish"    